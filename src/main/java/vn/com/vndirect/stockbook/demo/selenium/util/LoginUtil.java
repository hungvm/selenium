package vn.com.vndirect.stockbook.demo.selenium.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertEquals;

public class LoginUtil {
    public static void checkLoginSuccessfully(WebDriver webDriver, WebDriverWait webDriverWait) {
        WebElement tab = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("tab-t1-2")));
        String text = tab.getText();
        assertEquals("Cả cộng đồng", text);
    }
}
