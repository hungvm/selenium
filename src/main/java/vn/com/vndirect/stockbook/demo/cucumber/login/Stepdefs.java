package vn.com.vndirect.stockbook.demo.cucumber.login;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import vn.com.vndirect.stockbook.demo.Utils;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertEquals;

public class Stepdefs {
    private WebDriver webDriver;
    private WebDriverWait webDriverWait;

    @Given("^Open browser and start application$")
    public void openBrowserAndStartApp() throws MalformedURLException {
        DesiredCapabilities capability = DesiredCapabilities.firefox();
//        webDriver = new FirefoxDriver();
        webDriver = new RemoteWebDriver(new URL("http://127.0.0.1:4444"), capability);
        webDriver.manage().window().maximize();
        webDriverWait = new WebDriverWait(webDriver, 10);
        webDriver.get("https://stockbook.vn/v1");
        Utils.sleep(100);
    }

    @When("^Chosen login using VND and enter exist username password$")
    public void chosenVNDAndEnterUserNameAndPassword() {
        WebElement vndIcon = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@src='assets/img/logo-VND.png']")));
        WebElement vndLoginButton = vndIcon.findElement(By.xpath(".."));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(vndLoginButton));
        vndLoginButton.click();

        webDriver.findElement(By.id("username")).sendKeys("thuypham@gmail.com");
        webDriver.findElement(By.id("password")).sendKeys("thuychip");
        webDriver.findElement(By.className("btnLogin")).click();
    }

    @Then("^User should login successfully$")
    public void shouldUserLoginSuccessfully() {
        WebElement tab = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("tab-t1-2")));
        String text = tab.getText();
        System.out.println(text);
        assertEquals("Cả cộng đồng", text);
    }

}
