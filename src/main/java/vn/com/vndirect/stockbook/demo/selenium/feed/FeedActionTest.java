package vn.com.vndirect.stockbook.demo.selenium.feed;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import vn.com.vndirect.stockbook.demo.Utils;
import vn.com.vndirect.stockbook.demo.selenium.core.FirefoxLoginedTest;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

/**
 * Test các case trong màn hình sau khi đăng nhập
 * Ví dụ: Click bình luận ra chi tiết tút, click gửi tút ra màn hình gửi tút, ...
 */
public class FeedActionTest extends FirefoxLoginedTest {
    @BeforeClass
    public static void init() {
        FirefoxLoginedTest.init();
    }

    @Before
    public void setup() {
        super.setup();
    }

    @Test
    public void testClickCommentButton_goToPostDetail() {
        List<WebElement> commentBtns = webDriver.findElements(By.xpath("//*[@id='tabpanel-t1-2']/page-feed/ion-content/div[2]/div/ion-card[1]/post-item/div/post-action/button[2]"));
        assertTrue(commentBtns.size() > 0);
        WebElement commentButton = commentBtns.get(0);
        WebElement tus = commentButton.findElement(By.xpath("../.."));
        WebElement tusContent = tus.findElement(By.cssSelector("span[class='content']"));
        String tusText = tusContent.getText();
        Utils.scrollToElement(webDriver, commentButton);
        Actions actions = new Actions(webDriver);
        actions.moveToElement(commentButton).click().build().perform();
        Utils.sleep(1000);
        WebElement commentTitle = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class='toolbar-title toolbar-title-md']")));
        assertNotNull("must not null", commentTitle);
        System.out.println(commentTitle.getText());
        assertEquals("Bình luận", commentTitle.getText());
        WebElement commentContent = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='tabpanel-t0-0']/page-comment/ion-content/div[2]/ion-list/ion-card[1]/post-item/div/content-view/div/span/span")));
        System.out.println(commentContent.getText());
        assertEquals("text must same", tusText, commentContent.getText());

    }

    @Test
    public void testOpenFeedMa(){
        WebElement maBtn = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("tab-t0-1")));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(maBtn));
        maBtn.click();
        Utils.sleep(3000);
        WebElement tab = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ion-tab[@id='tabpanel-t0-1']/interested-code/ion-header/ion-navbar/div[2]/ion-title/div")));
        String text = tab.getText();
        assertTrue("Can not find ma quan tam", text.contains("Mã quan tâm"));
    }

    @Test
    public void testOpenFeedNoti(){
        WebElement notiBtn = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("tab-t0-3")));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(notiBtn));
        notiBtn.click();
        Utils.sleep(3000);
        WebElement tab = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ion-tab[@id='tabpanel-t0-3']/page-notify/ion-header/ion-navbar/div[2]/ion-title/div")));
        String text = tab.getText();
        assertTrue(text.contains("Thông báo"));
    }

    @Test
    public void testOpenFeedStockbook(){
        WebElement sbBtn = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("tab-t0-0")));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(sbBtn));
        sbBtn.click();
        Utils.sleep(3000);
        WebElement tab = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ion-tab[@id='tabpanel-t0-0']/page-main/ion-header/ion-navbar/div[2]/ion-title/div")));
        String text = tab.getText();
        System.out.print(text);
        assertEquals("Stockbook", text);
    }

    @Test
    public void testOpenMenu(){
        WebElement menuBtn = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("tab-t0-4")));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(menuBtn));
        menuBtn.click();
        Utils.sleep(3000);
        WebElement tab = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//ion-tab[@id='tabpanel-t0-4']/page-menu/ion-header/ion-navbar/div[2]/ion-title/div")));
        String text = tab.getText();
        System.out.print(text);
        assertEquals("Menu", text);
    }

    @Test
    public void testSeen(){
        List<WebElement> seenBtns = webDriver.findElements(By.xpath("//span[contains(text(),'Đã xem')]"));
        assertTrue(seenBtns.size() >0);
        WebElement seenButton = seenBtns.get(0);
        String text = seenButton.getText();
        assertNotNull("must not null", text);
        String[] arr = text.split(" ");
        int seenNumber = Integer.parseInt(arr[0]);
        assertTrue("seen number must > 0", seenNumber > 0);
    }

    @Test
    public void testLikeStt(){
        List<WebElement> likeBtns = webDriver.findElements(By.xpath("//span[contains(text(),'Thích')]"));
        assertTrue(likeBtns.size() >0);
        WebElement likeButton = likeBtns.get(0);
        WebElement tus = likeButton.findElement(By.xpath("../../../.."));
        WebElement tusContent = tus.findElement(By.cssSelector("span[class='content']"));
        Utils.sleep(3000);
        String tusText = tusContent.getText();
        Utils.scrollToElement(webDriver, likeButton);
        Actions actions = new Actions(webDriver);
        actions.moveToElement(likeButton).click().build().perform();
        Utils.sleep(1000);

        WebElement commentTitle = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class='toolbar-title toolbar-title-md']")));
        assertNotNull("must not null", commentTitle);
        assertEquals("Thích", commentTitle.getText());

        WebElement commentContent = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("span[class='content']")));
        assertEquals("text must same", tusText, commentContent.getText());
    }



    @AfterClass
    public static void destroy() {
        FirefoxLoginedTest.destroy();
    }
}
