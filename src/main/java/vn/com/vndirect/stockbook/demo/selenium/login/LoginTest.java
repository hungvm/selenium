package vn.com.vndirect.stockbook.demo.selenium.login;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import vn.com.vndirect.stockbook.demo.Utils;
import vn.com.vndirect.stockbook.demo.selenium.Constant;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

;

public class LoginTest {
    private final String URL = Constant.WEB_URL;
    private WebDriver webDriver;
    private WebDriverWait webDriverWait;

    @Before
    public void setup() {
        webDriver = new FirefoxDriver();
        webDriver.manage().window().maximize();
        webDriverWait = new WebDriverWait(webDriver, 10);
        webDriver.get(URL);
        Utils.sleep(100);
    }

    @After
    public void teardown() {
        webDriver.close();
        Utils.sleep(100);
    }

    @Test
    public void testLoginWithVND_whenUserPassIsValid() {

        WebElement vndIcon = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@src='assets/img/logo-VND.png']")));
        WebElement vndLoginButton = vndIcon.findElement(By.xpath(".."));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(vndLoginButton));
        vndLoginButton.click();

        webDriver.findElement(By.id("username")).sendKeys("thuypham@gmail.com");
        webDriver.findElement(By.id("password")).sendKeys("thuychip");
        webDriver.findElement(By.className("btnLogin")).click();

        checkLoginSuccess();

    }

    @Test
    public void testLoginWithGoogle() {
        String EMAIL = "";
        String PASSWORD = "";
        WebElement googleBtn = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.className("google")));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(googleBtn));
        googleBtn.click();
        googleBtn.click();

        String currentWindow = webDriver.getWindowHandle();
        switchToNextWindow(currentWindow);

        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("identifierId"))).sendKeys(EMAIL);
        webDriver.findElement(By.id("identifierNext")).click();

        Utils.sleep(1000);

        WebElement password = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@type='password']")));
        password.sendKeys(PASSWORD);

        webDriver.findElement(By.id("passwordNext")).click();

        webDriver.switchTo().window(currentWindow);

        checkLoginSuccess();
    }

    private String switchToNextWindow(String currentWindow) {

        webDriverWait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                return webDriver.getWindowHandles().size() > 1;
            }
        });
        String popupWindow = null;
        for (String window : webDriver.getWindowHandles()) {
            if (!window.equals(currentWindow)) {
                popupWindow = window;
                break;
            }
        }
        if (popupWindow == null) {
            assertFalse("can not find pop up window", true);
        }
        webDriver.switchTo().window(popupWindow);
        return popupWindow;
    }

    @Test
    public void testLoginWithFacebook() {
        String EMAIL = "";
        String PASS = "";
        Utils.webDriverSleep(webDriver, 1000);
        WebElement facebookBtn = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.className("facebook")));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(facebookBtn));

        facebookBtn.click();

        String currentWindow = webDriver.getWindowHandle();
        switchToNextWindow(currentWindow);

        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("email"))).sendKeys(EMAIL);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("pass"))).sendKeys(PASS);
        webDriver.findElement(By.id("u_0_0")).click();

        webDriver.switchTo().window(currentWindow);

        checkLoginSuccess();

    }

    private void checkLoginSuccess() {
        WebElement tab = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("tab-t1-2")));
        String text = tab.getText();
        System.out.println(text);
        assertEquals("Cả cộng đồng", text);
    }


}
