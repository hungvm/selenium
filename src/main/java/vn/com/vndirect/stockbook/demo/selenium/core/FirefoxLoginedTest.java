package vn.com.vndirect.stockbook.demo.selenium.core;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import vn.com.vndirect.stockbook.demo.Utils;
import vn.com.vndirect.stockbook.demo.selenium.Constant;

import static org.junit.Assert.assertEquals;

public class FirefoxLoginedTest {
    protected static WebDriver webDriver;
    protected static WebDriverWait webDriverWait;
    private static final String URL = Constant.WEB_URL;

    @BeforeClass
    public static void init() {
        webDriver = new FirefoxDriver();
        webDriver.manage().window().maximize();
        webDriverWait = new WebDriverWait(webDriver, 10);

        webDriver.get(URL);
        Utils.sleep(300);

        WebElement vndIcon = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@src='assets/img/logo-VND.png']")));
        WebElement vndLoginButton = vndIcon.findElement(By.xpath(".."));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(vndLoginButton));

        vndLoginButton.click();

        webDriver.findElement(By.id("username")).sendKeys("thuypham@gmail.com");
        webDriver.findElement(By.id("password")).sendKeys("thuychip");
        webDriver.findElement(By.className("btnLogin")).click();

        checkLoginSuccess();
    }

    private static void checkLoginSuccess() {
        WebElement tab = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id("tab-t1-2")));
        String text = tab.getText();
        System.out.println(text);
        assertEquals("Cả cộng đồng", text);
    }

    @Before
    public void setup() {
        webDriver.get(Constant.WEB_URL);
        Utils.sleep(100);
    }

    @AfterClass
    public static void destroy() {
        webDriver.quit();
    }
}
