package vn.com.vndirect.stockbook.demo.selenium.feed;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import vn.com.vndirect.stockbook.demo.Utils;
import vn.com.vndirect.stockbook.demo.selenium.core.FirefoxLoginedTest;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;

public class PostDetailTest extends FirefoxLoginedTest {
    @BeforeClass
    public static void init() {
        FirefoxLoginedTest.init();

        goToPostDetail();
    }

    @Before
    public void setup() {
        super.setup();
        Utils.sleep(300);
    }

    private static void goToPostDetail() {
        List<WebElement> commentBtns = webDriver.findElements(By.xpath("//span[contains(text(),'Bình luận')]"));
        assertTrue(commentBtns.size() > 0);
        WebElement commentButton = commentBtns.get(0);
        Utils.scrollToElement(webDriver, commentButton);
        Actions actions = new Actions(webDriver);
        actions.moveToElement(commentButton).click().build().perform();

        WebElement commentTitle = webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[class='toolbar-title toolbar-title-md']")));
        assertNotNull("must not null", commentTitle);
        assertEquals("Bình luận", commentTitle.getText());
    }

    @Test
    public void testAddCommentForPost_reloadShouldHaveComment() {
        // add comment and click send
        //reload and go to post detail
        webDriver.navigate().refresh();
        goToPostDetail();
        // check comment is exist
    }
}
